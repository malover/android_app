package com.example.coursera.cameraapp;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import junit.framework.Assert;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Alex on 12.07.2016.
 */
class FileUtils {
    private static final int MEDIA_TYPE_IMAGE = 1;
    private static final String TARGET_FOLDER = "MyCameraApp";

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFileExternalStorage(int type, Context ctx) {
        // To be safe, check that the SDCard is mounted

        if (Environment.getExternalStorageState().equalsIgnoreCase("mounted")) {

            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "MyCameraApp");
            // This location works best if you want the created images to be shared
            // between applications and persist after your app has been uninstalled.

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("MyCameraApp", "failed to create directory");
                    return null;
                }
            }

            // Create a media file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File mediaFile;
            if (type == MEDIA_TYPE_IMAGE) {
                mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                        "IMG_" + timeStamp + ".png");
            } else {
                Assert.fail("SD card is unavailable.");
                return null;
            }

            return mediaFile;
        } else {
            Assert.fail("SD card is unavailable.");
            return null;
        }
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFileInternalStorage(int type, Context ctx) {
            ContextWrapper cw = new ContextWrapper(ctx);
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            File mediaStorageDir = new File(directory, TARGET_FOLDER);
            // This location works best if you want the created images to be shared
            // between applications and persist after your app has been uninstalled.

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("MyCameraApp", "failed to create directory");
                    return null;
                }
            }
            // Create a media file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File mediaFile;
            if (type == MEDIA_TYPE_IMAGE) {
                mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                        "IMG_" + timeStamp + ".png");
            } else {
                Assert.fail("Internal Storage is unavailable.");
                return null;
            }
            return mediaFile;
    }


    public static File saveImage(Bitmap bmp, Context ctx) {
        File file = getOutputMediaFileInternalStorage(MEDIA_TYPE_IMAGE, ctx);
        try {
            OutputStream outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
            return null;
        }
        return file;

    }

}

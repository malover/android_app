package com.example.coursera.cameraapp;

/**
 * Created by Alex on 15.07.2016.
 */
public enum RequestCodes {
    ZOOM_CODE(33), CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE(100);

    int codeValue;

    RequestCodes(int v) {
        this.codeValue = v;
    }

    public int getCodeValue() {
        return codeValue;
    }
}

package com.example.coursera.cameraapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.coursera.myapplication.R;

/**
 * Created by a.lyubozhenko on 14.07.16.
 */
public class ViewActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        ImageView imageView = (ImageView) findViewById(R.id.expanded_imagination);

        Bundle extras = getIntent().getExtras();
        loadBitmap((String) extras.get("path"), imageView);
        Toast.makeText(this, "Touch the screen to return to the previous window" , Toast.LENGTH_SHORT).show();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    private void loadBitmap(String path, ImageView imageView) {
        BitmapWorkerTask task = new BitmapWorkerTask(imageView);
        task.execute(path);
    }

}

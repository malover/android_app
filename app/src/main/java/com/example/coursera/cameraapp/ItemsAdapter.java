package com.example.coursera.cameraapp;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.coursera.myapplication.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Alex on 12.07.2016.
 */
class ItemsAdapter extends ArrayAdapter<File> {
    private Bitmap bitmap = null;
    private String path;
    ViewHolder viewHolder;
    private ArrayList<File> items;

    public ArrayList<File> getItems() {
        return items;
    }

    ItemsAdapter(Context context, ArrayList<File> items) {
        super(context, R.layout.images_list, items);
        this.items = items;
    }

    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        ImageButton imageView = null;
        TextView textView = null;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        calendar.setTimeInMillis(this.getItem(position).lastModified());

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Service.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.images_list, null);
            viewHolder = new ViewHolder();
            if (convertView != null) {
                path = this.getItems().get(position).getAbsolutePath();
                imageView = (ImageButton) convertView.findViewById(R.id.imageView);
                viewHolder.setImage(imageView);
                viewHolder.setPath(path);
                textView = (TextView) convertView.findViewById(R.id.textView);
                viewHolder.setText(textView);
                convertView.setTag(viewHolder);
            }
        }

        else{
            viewHolder = (ViewHolder) convertView.getTag();
            imageView = viewHolder.getImage();
            textView =  viewHolder.getText();
            path = viewHolder.getPath();
        }
            loadBitmap(path, imageView);
            textView.setText("Selfie shot on: " + String.valueOf(calendar.getTime().toString()));
            textView.setVisibility(View.VISIBLE);

        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                ViewHolder holder = (ViewHolder) v.getTag();
                Intent intent = new Intent(getContext(), ViewActivity.class);
                intent.putExtra("path", holder.getPath());
                ((Activity)getContext()).startActivityForResult(intent, RequestCodes.ZOOM_CODE.getCodeValue());
             }
        });

        return convertView;
    }

    private class ViewHolder{
        public void setImage(ImageButton image) {
            this.image = image;
        }

        public void setText(TextView text) {
            this.text = text;
        }

        public ImageButton getImage() {
            return image;
        }

        public TextView getText() {
            return text;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }
        private ImageButton image;
        private TextView text;
        private String path;
    }

    private void loadBitmap(String path, ImageView imageView) {
        BitmapWorkerTask task = new BitmapWorkerTask(imageView);
        task.execute(path);
    }


}

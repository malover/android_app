package com.example.coursera.cameraapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.example.coursera.cameraapp.alarm.AlarmNotificationReceiver;
import com.example.coursera.myapplication.R;

import junit.framework.Assert;

import java.io.File;
import java.util.ArrayList;

public class CameraActivity extends ListActivity {
    private Intent intent;
    private ItemsAdapter adapter;
    private ArrayList<File> savedImages;
    private AlarmManager mAlarmManager;
    private Intent mNotificationReceiverIntent;
    private PendingIntent mNotificationReceiverPendingIntent;
    private boolean alarmIsSet;
    private static final long INITIAL_ALARM_DELAY = 1 * 20 * 1000L;
    public static final String DATA_REFRESHED_ACTION = "DATA_REFRESHED";
    public static final int IS_ALIVE = Activity.RESULT_FIRST_USER;
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        if(null!=savedInstanceState){
            onRestoreInstanceState(savedInstanceState);
        }
        setContentView(R.layout.activity_camera);

/******* Finished Alarm set up*/
        //prepare empty list for Image files.
        savedImages = new ArrayList<>();

        listView = getListView();
        //adapter to operate with files and their View-based representation
        adapter = new ItemsAdapter(this, getLoadedItems());
        listView.setAdapter(adapter);
        // create Intent to take a picture and return control to the calling application
        intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
/******** Setting up the Alarm settings*/
//        setAlarmSettings();
        Switch alarmSwitchButton = (Switch) findViewById(R.id.switch1);
        alarmIsSet = alarmSwitchButton.isChecked();
        alarmSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if((isChecked)&&(alarmIsSet!=isChecked)){
                    setAlarmSettings();
                }
                else{
                    cancelAlarm();
                }
            }
        });

        // So called 'Make New Photo' button
        ImageButton captureButton = (ImageButton) findViewById(R.id.button);
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (checkSelfPermission(Manifest.permission.CAMERA)
                                    != PackageManager.PERMISSION_GRANTED) {

                                requestPermissions(new String[]{Manifest.permission.CAMERA},
                                        13);
                            }
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        13);
                            }
                        }
                        startActivityForResult(intent, RequestCodes.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE.getCodeValue());
                    }
                }
        );
    }

    // This method is run, after new shot was made.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodes.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE.getCodeValue()) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");

                File savedFile = FileUtils.saveImage(imageBitmap, getApplicationContext());
                adapter.add(savedFile);

                Toast.makeText(this, "Image saved to:\n" +
                        savedFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
                Toast.makeText(this, "Image capture failed" , Toast.LENGTH_LONG).show();
                Assert.fail("Image capture failed");
            }
        }

        else if ((RequestCodes.ZOOM_CODE.getCodeValue() == requestCode)&&(resultCode == RESULT_OK)){
            Toast.makeText(this, "Just look in my eyes and you'll see Selfie Paradise..." , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

    }

    private ArrayList<File> getLoadedItems() {
        return savedImages;
    }

    //////// Notification///////

//    private void setupBroadcast() {
//
//        // The feed is fresh if it was downloaded less than 2 minutes ago
//        mIsFresh = (System.currentTimeMillis() - lastModified) < TWO_MIN;
//        if (!mIsFresh) {
//            // Set up a BroadcastReceiver to receive an Intent when download
//            // finishes.
//            mRefreshReceiver = new BroadcastReceiver() {
//                @Override
//                public void onReceive(Context context, Intent intent) {
//
//                    if (mRefreshReceiver.isOrderedBroadcast() && intent.getAction().equals(DATA_REFRESHED_ACTION)) {
//                        setResultCode(CameraActivity.IS_ALIVE);
//                    }
//                }
//            };
//        }
//    }

    private void setAlarmSettings(){
        // Get the AlarmManager Service
        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        // Create an Intent to broadcast to the AlarmNotificationReceiver
        mNotificationReceiverIntent = new Intent(CameraActivity.this,
                AlarmNotificationReceiver.class);

        // Create an PendingIntent that holds the NotificationReceiverIntent
        mNotificationReceiverPendingIntent = PendingIntent.getBroadcast(
                CameraActivity.this, 0, mNotificationReceiverIntent, 0);

        mAlarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + INITIAL_ALARM_DELAY,
//                AlarmManager.INTERVAL_FIFTEEN_MINUTES,
                INITIAL_ALARM_DELAY,
                mNotificationReceiverPendingIntent);
    }

    private void cancelAlarm(){
        // Get the AlarmManager Service
        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        // Create an Intent to broadcast to the AlarmNotificationReceiver
        mNotificationReceiverIntent = new Intent(this,
                AlarmNotificationReceiver.class);
        // Create an PendingIntent that holds the NotificationReceiverIntent
        mNotificationReceiverPendingIntent = PendingIntent.getBroadcast(
                this, 0, mNotificationReceiverIntent, 0);
        mAlarmManager.cancel(mNotificationReceiverPendingIntent);
    }


}
